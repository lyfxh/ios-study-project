//
//  AppDelegate.h
//  Meizi
//
//  Created by Sunnyyoung on 15/4/4.
//  Copyright (c) 2015年 Sunnyyoung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>

//AppDelegate调用UIResponder来处理Ios事件。
//完成UIApplication的命令，提供关键应用程序事件，如启动完毕，终止，等等
@interface AppDelegate : UIResponder <UIApplicationDelegate>

//在iOS设备的屏幕上用UIWindow对象来管理和协调各种视角，它就像其它加载视图的基本视图一样。通常一个应用程序只有一个窗口。
@property (strong, nonatomic) UIWindow *window;

@end

